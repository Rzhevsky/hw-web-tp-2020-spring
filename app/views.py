from django.http import HttpResponse, HttpRequest
from django.shortcuts import render

lenPreText = 200

numberOfQuestions = 25

numberOfAnswers = 10

objectsPerPage = 5

questions = {
	 i : {'id': i, 
		  'title' : f'question # {i}',
		  'question' : 'How to build a moon park?',
		  'text' : 'Lorem ipsum dolor sit amet, consectetur \
				adipiscing elit, sed do eiusmod tempor incididunt \
				ut labore et dolore magna aliqua Ut enim ad minim \
				veniam, quis nostrud exercitation ullamco laboris \
				nisi ut aliquip ex ea commodo consequat. Duis aute \
				irure dolor in reprehenderit in voluptate velit esse \
				cillum dolore eu fugiat nulla pariatur. Excepteur \
				sint occaecat cupidatat non proident, sunt in culpa \
				qui officia deserunt mollit anim id est laborum.',
		   'tags' : ['html', 'django', 'MVC'],
		}
	 for i in range(numberOfQuestions)
}

for i in range(numberOfQuestions):
	questions[i]['pretext'] = questions[i]['text'][:lenPreText:]


answers = [
	{
		'id' : i,
		'text': 'First of all I woulb like to thank you \
			for the invitation to participate is such a ... \
			Russia is the huge territory which in many respects \
			needs to be render habitable.',
	} 
	for i in range(numberOfAnswers)
]


from django.core.paginator import Paginator, InvalidPage

numberOfPaginationButtons = 5

def paginate(object_list, request):
	numberOfPaginationButtons = len(object_list) / objectsPerPage

	paginator = Paginator(object_list, objectsPerPage)

	page = request.GET.get('page')
	object_page = paginator.get_page(page)

	l2 = [object_page.number]

	l1 = [(object_page.number - i) for i in range(1,3) if i < object_page.number]
	
	l1.reverse()

	l3 = [(object_page.number + i) for i in range(1,3) if 
							(object_page.number + i) <= numberOfPaginationButtons]

	result_bound = l1 + l2 + l3

	prevPage, nextPage = (0,0)
	if object_page.has_previous():
		prevPage = object_page.previous_page_number()
	else:
		prevPage = int(object_page.number) - 1

	if object_page.has_next():
		nextPage = object_page.next_page_number()
	else: 
		nextPage = int(object_page.number) + 1

	result_dict = {'bounds' : result_bound,
				    'prev_page' : prevPage,
				    'next_page' : nextPage
				   }


	return object_page, result_dict


# Create your views here.

def index(request):
	questions_page, paginate_data = \
							paginate(list(questions.values()), request)

	return render(request, 'index.html', {
		'questions' : questions_page,
		'bounds' : paginate_data['bounds'],
		'page' : int(questions_page.number),
		'prev' : paginate_data['prev_page'],
		'next' : paginate_data['next_page'],
		})

def login(request):
	return render(request, 'login.html', {})

def signup(request):
	return render(request, 'signup.html', {})


def question(request, qid):
	question = questions.get(qid)

	answer_page, paginate_data = \
						paginate(answers, request)

	return render(request, 'question.html', {
			'question' : question,
			'bounds' : paginate_data['bounds'],
			'answers' : answer_page,
			'page' : answer_page.number,
			'prev' : paginate_data['prev_page'],
			'next' : paginate_data['next_page'],
		})

def tag(request, tag):
	matched_questions = list()

	for question in questions.values():
		if tag in question['tags']:
			matched_questions.append(question)

	return render(request, 'tag.html', {
			'questions' : matched_questions,
			'tag' : tag,
		})

def hot(request):
	return render(request, 'hot.html', {
			'questions' : questions.values(),
		})

def ask(request):
	return render(request, 'ask.html', {})